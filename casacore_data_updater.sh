#A simple shell script to update casacore data / LOFAR beam models 

#Inside PulsarLiveGLOWbeta casacore data is stored in:
cd /pulsar_software/

#try the --timestamping method to get the latest measures files 
wget --show-progress --continue --timestamping ftp://ftp.astron.nl/outgoing/Measures/WSRT_Measures.ztar

#untar-ing is all that is required to install the measures
tar -xzvf ../Downloads/WSRT_Measures.ztar
